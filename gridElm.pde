interface grid {
  void activate(int x, int y); 
  void initialPosition(int x, int y);
  void draw();
}
public class GridElement implements grid {
  int x, y, m;
  int gridNum;
  int varient;
  int LcolorON, Lcolor2, Lcolor3;
  int pagenum;
  boolean changeVarientUp, changeVarientDown;

  boolean gridPressed = false;
  LColor currentColor;

  GridElement(int initgridNum, int initpagenum) {
    gridNum = initgridNum;
    pagenum = initpagenum;
    currentColor = new LColor(3, 0, 0);
  }
  void draw() {
    //println(gridNum+"   "+x+"    "+y);
  }
  public void movePad(int initmovea) {

    int movea = initmovea;

    if (movea == 1000) { y-=1;}
    if (movea == 0100) { y+=1;}
    if (movea == 0010) { x-=1;}
    if (movea == 0001) { x+=1;}
    if (movea == 0000) { x=x; }

    if ( x >= 8 ) { x = 0;}
    if ( x < 0) { x = 7;}
    if ( y >= 8 ) { y = 0;}
    if ( y < 0) { y = 7; }
    
    if ((gridPressed == true) && (x == 7)) {
            triggerSample(varient,y,pagenum);  
          //  println(gridElm[i].x);
         }
  }
  public void movePadBackward() {
    x -= 1;
  }
  public void initialPosition(int initx, int inity) {
    x = initx;
    y = inity;
    LcolorON = LColor.GREEN_LOW;
  }  
  public void activate(int initx, int inity) {
    int checkX = initx;
    int checkY = inity;
    
    if ((checkX == x) && (checkY == y)) {
     
      if ((changeVarientUp == true)) {
        if(gridPressed == true){
        varientColors(1);
        }
      } else if ((changeVarientDown == true)) {
        if(gridPressed == true){
        varientColors(-1);
        }
      } else {
        
        gridPressed = !gridPressed;
        applyState(); 
        
      }

      //println(gridPressed);
      //launchpad.changeGrid(x, y, LColor.GREEN_HIGH);
    }
  }
  public void applyState() {   
    if (gridPressed == true) {
      launchpad.changeGrid(x, y, LColor.GREEN_LOW);
    } 
    else {
      launchpad.changeGrid(x, y, LColor.OFF);
    }
  }
  public void varientColors(int increment) {  

    //if (gridPressed == true) {
    varient += increment;
    if (varient > 4) {
      varient = 3;
    }
    if (varient < 0) {
      varient = 0;
    }
    switch(varient) {
    case 0:
      LcolorON = LColor.GREEN_LOW;
      break;
    case 1:
      LcolorON = LColor.GREEN_HIGH;
      break;
    case 2:
      LcolorON = LColor.YELLOW_LOW;
      break;
    case 3:
      LcolorON = LColor.YELLOW_HIGH;
      break;  
  }
    //}
  }
  public int colorPad() {

    if ( gridPressed == true ) {
      return LcolorON;
    } 
    else {
      return LColor.OFF;
    }
  }
 
}

