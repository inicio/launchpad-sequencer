public class Button {

  int i, code, x, y;
  int lcolorON, lcolorOFF, lcolorPRESS;
  boolean buttonPressed;

  Button(int initi) {
    i = initi;
  }
  public void initialPosition(int initcode) {
    code = initcode;
    assignColors();
  } 
  public void activate(int initcode) {
    int checkCode = initcode;
    
    if (checkCode == code) {
      buttonPressed = !buttonPressed;
      applyState();
    }
    
  }
  public void applyState() {   
    println("i="+i);
    if (buttonPressed == true) {
      if( i < 8 ){
      launchpad.changeSceneButton(code, lcolorPRESS);
      } else {
      launchpad.changeButton(code, lcolorPRESS);
      }
    } 
    else {
      if( i < 8 ){
      launchpad.changeSceneButton(code, lcolorOFF);
      } else {
      launchpad.changeButton(code, lcolorOFF);
      }
    }
  }
  public void assignColors() {  // assigned at initialization, only runs once
    switch(code) {
    case LButton.LEFT:
    case LButton.UP:
    case LButton.DOWN:
    case LButton.RIGHT:
      lcolorPRESS = LColor.RED_MEDIUM;
      lcolorON = LColor.RED_HIGH;
      lcolorOFF = LColor.RED_LOW;
      break;
    case LButton.SESSION:
      lcolorPRESS = LColor.YELLOW_MEDIUM;
      lcolorON = LColor.YELLOW_HIGH;
      lcolorOFF = LColor.YELLOW_LOW;
      break;
    case LButton.SCENE1:
    case LButton.SCENE2:
      lcolorPRESS = LColor.YELLOW_MEDIUM;
      lcolorON = LColor.YELLOW_HIGH;
      lcolorOFF = LColor.YELLOW_LOW;
      break;
    }
  }
  public int colorPad() {
    if ( buttonPressed == true ) {
      return lcolorON;
    } else {
      return lcolorOFF;
    }
  }
}
