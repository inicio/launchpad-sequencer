import controlP5.*;

import ddf.minim.*;
import ddf.minim.signals.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;

import com.rngtng.launchpad.*;
import themidibus.*;
import jklabs.monomic.*;
import processing.serial.*;

Launchpad launchpad;
ControlP5 gui;
Minim minim;
AudioSample sample1, sample2, sample3, sample4, sample5, sample6, sample7, sample0;
AudioSample drum1, drum2, drum3, drum4, drum5, drum6;
GridElement[] gridElm = new GridElement[64];
GridElement[] gridElm2 = new GridElement[64];
Button[] button = new Button[16];

int mode;
int arrayIndex;
int moveCode;

int page;

public int bpm, tempo, clock;
boolean freeze = false;
boolean varup = false;
boolean sampleTrigger = false;
float timer;

ArrayList drawList = new ArrayList();

void setup() {
  launchpad = new Launchpad(this); 
  launchpad.reset(); 

  minim = new Minim(this);
  sample0 = minim.loadSample("1.wav", 512);  
  sample1 = minim.loadSample("2.wav", 512);
  sample2 = minim.loadSample("3.wav", 512);
  sample3 = minim.loadSample("4.wav", 512);
  sample4 = minim.loadSample("5.wav", 512);
  sample5 = minim.loadSample("6.wav", 512);
  sample6 = minim.loadSample("7.wav", 512);
  sample7 = minim.loadSample("8.wav", 512);
  
  sample0.setGain(-10.0);
  sample1.setGain(-10.0);
  sample2.setGain(-10.0);
  sample3.setGain(-10.0);
  sample4.setGain(-10.0);
  sample5.setGain(-10.0);
  sample6.setGain(-10.0);
  sample7.setGain(-10.0);
  
  drum1 = minim.loadSample("kick.wav",512);
  drum1.setGain(-5.0);
  drum2 = minim.loadSample("snare.wav",512);
  drum3 = minim.loadSample("clap.wav",512);
  
  drum4 = minim.loadSample("Beautiful_Stars.wav",512);
  drum5 = minim.loadSample("Hey.wav",512);
  drum6 = minim.loadSample("Look_at_ya.wav",512);
  

  size(300, 300);
  gui = new ControlP5(this);
  gui.addNumberbox("bpm", 120, 10, 5, 20, 15);
  clock = millis();
  bpm = 120;
  tempo = 125;

  frameRate(5);
  int buttonindex = 104;
  int scenebuttonindex = 9;
  for (int i = 0; i < button.length; i++) {
    button[i] = new Button(i);
  }
  for (int i = 0; i < button.length; i++) {
    if (scenebuttonindex < 122) {
      button[i].initialPosition(scenebuttonindex);  
      scenebuttonindex += 16;
    } 
    else {
      button[i].initialPosition(buttonindex);  
      buttonindex += 1;
    }
    print(button[i].code+"  ");
  }
  for (int i = 0; i < gridElm.length; i++) {
    gridElm[i] = new GridElement(i,0);
    drawList.add(gridElm[i]);
  }
  for (int i = 0; i < gridElm.length; i++) {
    gridElm2[i] = new GridElement(i,1);
  }
  int index = 0;
  for (int x = 0; x <= 7; x++) {
    for (int y = 0; y <= 7; y++) {

      gridElm[index].initialPosition(x, y);
      gridElm2[index].initialPosition(x, y);
      index += 1;
    }
  }
  moveCode = 0001;
}
void draw() {
  int[] LArrayColors = new int[80];
  int storemoveCode = moveCode;

  if ((millis() - clock >= tempo) || (timer == 1)) {
    clock = millis();
    sampleTrigger = false;
  }
  if ( (!sampleTrigger) ) {
    for (int i = 0; i < gridElm.length; i++) {   
      gridElm[i].movePad(moveCode);
      gridElm2[i].movePad(moveCode);
    } 
    sampleTrigger =true;
  }
   for (int i = 0; i < gridElm.length; i++) { 
      if(page == 0){
        int x = gridElm[i].x;
        int y = gridElm[i].y;
        arrayIndex = x + y * 8;
        LArrayColors[arrayIndex] = gridElm[i].colorPad();
      } else if (page == 1){
        int x = gridElm2[i].x;
        int y = gridElm2[i].y; 
        arrayIndex = x + y * 8;
        LArrayColors[arrayIndex] = gridElm2[i].colorPad();
      }      
    
  }
  // Scene Buttons First
  for (int i = 0; i < button.length; i++) {
    int buttonIndex = 64 + i;
    LArrayColors[buttonIndex] = button[i].colorPad();
  }

  launchpad.changeAll(LArrayColors);
  gui.draw();
  println(page);
}



public void controlEvent(ControlEvent e) {
  if (e.controller().name() == "bpm" ) {
    float bps = (float)bpm/60.0f;
    tempo = int(1000/(bps * 4));
  }
    print(tempo+" ");
    print(bpm);
    println();
}
public void launchpadGridPressed(int x, int y) {
  for (int i = 0; i < gridElm.length; i++) {
    if(page == 0){
      gridElm[i].activate(x, y);
    } else if (page == 1){
      gridElm2[i].activate(x, y); 
    }
  }
}
/*
*
 *  Trigger Function
 *
 */
public void triggerSample(int varient, int y, int pagenum) {
  println(varient);
  print(y);
  if(pagenum == 0){
    switch(y) {
    case 1:sample1.trigger();break;
    case 2:sample2.trigger();break;
    case 3:sample3.trigger();break;
    case 4:sample4.trigger();break;
    case 5:sample5.trigger();break;
    case 6:sample6.trigger();break;
    case 7:sample7.trigger();break;
    case 0:sample0.trigger();break;
    }
  } else if( pagenum == 1){
    switch(y){
    case 1:drum1.trigger();break;
    case 2:drum2.trigger();break;
    case 3:drum3.trigger();break;
    case 4:drum4.trigger();break;
    case 5:drum5.trigger();break;
    case 6:drum6.trigger();break;
    
      }
  }
}
/*
*
 *  Button Functions
 *
 */
public void launchpadButtonPressed(int buttonCode) {

  timer = 1;
  /*for (int i = 0; i < gridElm.length; i++) {
   button[i].activate(buttonCode);
   }*/
  // following values are sent to gridElm function
  switch(buttonCode) {
  case LButton.LEFT:
    moveCode = 0010;
    break;
  case LButton.UP:
    moveCode = 1000;
    break;
  case LButton.DOWN:
    moveCode = 0100;
    break;
  case LButton.RIGHT:
    moveCode = 0001;
    break;
  case LButton.USER1:
    page = 0;
    break;
  case LButton.USER2:
    page = 1;
    break;
  case LButton.SESSION:
    freeze = !freeze;
    if (freeze == false) {
      moveCode = 0001;
    } 
    else {
      moveCode = 0000;
    }
    break;
  }

  for (int i = 0; i < button.length; i++) {
    button[i].activate(buttonCode);
  }
}  
public void launchpadButtonReleased(int buttonCode) {
  timer = 0;
  switch(buttonCode) {
  case LButton.LEFT:
  case LButton.UP:
  case LButton.DOWN:
  case LButton.RIGHT:
    if (freeze == false) {
      moveCode = 0001;
    } 
    else {
      moveCode = 0000;
    }
    break;
  case LButton.SESSION:
    freeze = !freeze;
    if (freeze == false) {
      moveCode = 0001;
    } 
    else {
      moveCode = 0000;
    }
    break;
  }
  for (int i = 0; i < button.length; i++) {
    button[i].activate(buttonCode);
  }
}
public int buttonNumber(int button) {
  return button;
}
/*
*
 *  Scene Functions
 *
 */
public void launchpadSceneButtonPressed(int buttonCode) {
  switch(buttonCode) {
  case LButton.SCENE1:
    for (int i = 0; i < gridElm.length; i++) {
      gridElm[i].changeVarientUp = true;
    }
    break;
  case LButton.SCENE2:
    for (int i = 0; i < gridElm.length; i++) {
      gridElm[i].changeVarientDown = true;
    }
    break;
  case LButton.SCENE3:
    bpm += 5;    
    break;
  case LButton.SCENE4:
    bpm -= 5;
    break;
  }
  float bps = (float)bpm/60.0f;
  tempo = int(1000/(bps * 4));
  for (int i = 0; i < button.length; i++) {
    button[i].activate(buttonCode);
    //println(button[i].buttonPressed);
  }
}
public void launchpadSceneButtonReleased(int buttonCode) {

  switch(buttonCode) {
  case LButton.SCENE1:
    for (int i = 0; i < gridElm.length; i++) {
      gridElm[i].changeVarientUp = false;
    }
    break;
  case LButton.SCENE2:
    for (int i = 0; i < gridElm.length; i++) {
      gridElm[i].changeVarientDown = false;
    }
    break;
  }

  for (int i = 0; i < button.length; i++) {
    button[i].activate(buttonCode);
    //println(button[i].buttonPressed);
  }
}
public int sceneButtonCode(int button) {
  return button;
}
void stop()
{
  // always close Minim audio classes when you are done with them
  minim.stop();
  
  super.stop();
}

